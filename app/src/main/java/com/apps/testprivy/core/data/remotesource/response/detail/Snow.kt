package com.apps.testprivy.core.data.remotesource.response.detail


data class Snow (

	val oneHour : Double
)