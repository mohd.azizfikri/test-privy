package com.apps.testprivy.core.data.remotesource.response

data class GeneralResponse<out T>(
    val code: Int,
    val message: String,
    val data: T
)