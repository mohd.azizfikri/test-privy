package com.apps.testprivy.core

import android.app.Application
import com.apps.testprivy.core.di.networkModule
import com.apps.testprivy.core.di.viewModelModule
import org.koin.core.context.startKoin

class CoreApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(
                listOf(
                    networkModule,
                    viewModelModule
                )
            )
        }
    }
}