package com.apps.testprivy.core.utils


const val BASE_URL = "http://api.openweathermap.org/"
const val SUB_URL = "data/2.5/"
const val APP_KEY = "1b7eeecd2ff64dc83e8dcf1f4cb2102b"

fun imageUrl(iconName: String): String{
    return "https://openweathermap.org/img/wn/${iconName}@2x.png"
}