package com.apps.testprivy.core.data

import com.apps.testprivy.core.data.remotesource.RemoteDataSource
import com.apps.testprivy.core.data.remotesource.network.ApiResponse
import com.apps.testprivy.core.data.remotesource.response.ForecastResponse
import com.apps.testprivy.core.data.remotesource.response.GeneralResponse
import com.apps.testprivy.core.data.remotesource.response.WeatherResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DataRepository(private val remoteDataSource: RemoteDataSource) {

    fun get5DaysForecastData(cityId: Int): Flow<Resource<GeneralResponse<ForecastResponse>?>> =
        flow {
            emit(Resource.Loading())
            try {
                remoteDataSource.get5DaysDataSource(cityId).collect {
                    val result = handleStateAPI(it)
                    emit(Resource.Success(result.data))
                }
            }catch (e: Exception){
                emit(Resource.Error(e.message.toString()))
            }
        }

    fun getCurrentWeatherData(cityName: String): Flow<Resource<GeneralResponse<WeatherResponse>?>> =
        flow {
            emit(Resource.Loading())
            try {
                remoteDataSource.getCurrentWeatherDataSource(cityName).collect {
                    val result = handleStateAPI(it)
                    emit(Resource.Success(result.data))
                }
            }catch (e: Exception){
                emit(Resource.Error(e.message.toString()))
            }
        }

    private fun <ResponseType> handleStateAPI(apiResponse: ApiResponse<ResponseType>): Resource<ResponseType> =
        when (apiResponse) {
            is ApiResponse.Success -> Resource.Success(apiResponse.data)
            is ApiResponse.Error -> Resource.Error(apiResponse.errorMessage)
            is ApiResponse.Empty -> Resource.Loading()
        }

    companion object {
        const val TAG = "DataRepository"
    }
}