package com.apps.testprivy.core.data.remotesource

import com.apps.testprivy.core.data.remotesource.network.ApiResponse
import com.apps.testprivy.core.data.remotesource.network.ApiService
import com.apps.testprivy.core.data.remotesource.response.ForecastResponse
import com.apps.testprivy.core.data.remotesource.response.GeneralResponse
import com.apps.testprivy.core.data.remotesource.response.WeatherResponse
import com.apps.testprivy.core.utils.APP_KEY
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun get5DaysDataSource(cityId: Int): Flow<ApiResponse<GeneralResponse<ForecastResponse>>> =
        flow {
            try {
                val response = apiService.get5DaysForecast(cityId, APP_KEY)
                emit(ApiResponse.Success(response))
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.message.toString()))
                e.printStackTrace()
            }
        }

    suspend fun getCurrentWeatherDataSource(cityName: String): Flow<ApiResponse<GeneralResponse<WeatherResponse>>> =
        flow {
            try {
                val response = apiService.getCurrentWeather(cityName, APP_KEY)
                emit(ApiResponse.Success(response))
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.message.toString()))
                e.printStackTrace()
            }
        }

    companion object {
        const val TAG = "RemoteDataSource"
    }
}