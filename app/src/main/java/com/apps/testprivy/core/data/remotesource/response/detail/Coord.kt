package com.apps.testprivy.core.data.remotesource.response.detail


data class Coord (

	val lon : Double,
	val lat : Double
)