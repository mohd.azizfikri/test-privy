package com.apps.testprivy.core.data.remotesource.response.detail


data class Weather (

	val id : Int,
	val main : String,
	val description : String,
	val icon : String
)