package com.apps.testprivy.core.data.remotesource.response

import com.apps.testprivy.core.data.remotesource.response.detail.*


data class WeatherResponse (
	val coord : Coord,
	val weather : List<Weather>,
	val base : String,
	val main : Main,
	val visibility : Int,
	val wind : Wind,
	val snow : Snow,
	val clouds : Clouds,
	val dt : Int,
	val sys : Sys,
	val timezone : Int,
	val id : Int,
	val name : String,
	val cod : Int
)