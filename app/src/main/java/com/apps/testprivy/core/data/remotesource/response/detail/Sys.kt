package com.apps.testprivy.core.data.remotesource.response.detail


data class Sys (

	val type : Int,
	val id : Int,
	val country : String,
	val sunrise : Int,
	val sunset : Int
)