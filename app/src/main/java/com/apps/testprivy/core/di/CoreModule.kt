package com.apps.testprivy.core.di

import com.apps.testprivy.core.data.DataRepository
import com.apps.testprivy.core.data.remotesource.RemoteDataSource
import com.apps.testprivy.core.data.remotesource.network.ApiService
import com.apps.testprivy.core.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val networkModule = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
    }

    single {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()

        retrofit.create(ApiService::class.java)
    }
}


val repositoryModule = module {
    single { RemoteDataSource(get()) }
    single { DataRepository(get()) }
}
