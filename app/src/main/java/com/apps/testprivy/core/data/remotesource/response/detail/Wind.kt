package com.apps.testprivy.core.data.remotesource.response.detail


data class Wind (

	val speed : Double,
	val deg : Int,
	val gust : Double
)