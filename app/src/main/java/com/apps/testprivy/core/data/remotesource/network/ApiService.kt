package com.apps.testprivy.core.data.remotesource.network

import com.apps.testprivy.core.data.remotesource.response.ForecastResponse
import com.apps.testprivy.core.data.remotesource.response.GeneralResponse
import com.apps.testprivy.core.data.remotesource.response.WeatherResponse
import com.apps.testprivy.core.utils.SUB_URL
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("$SUB_URL/product")
    suspend fun get5DaysForecast(
        @Query("id") cityId: Int,
        @Query("appid") appKey: String
    ): GeneralResponse<ForecastResponse>

    @GET("$SUB_URL/weather")
    suspend fun getCurrentWeather(
        @Query("q") cityName: String,
        @Query("appid") appKey: String
    ): GeneralResponse<WeatherResponse>

}