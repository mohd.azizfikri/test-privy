package com.apps.testprivy.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apps.testprivy.core.data.DataRepository
import kotlinx.coroutines.launch

class MainViewModel(private val repository: DataRepository) : ViewModel() {
    val isLogin by lazy { MutableLiveData<Boolean>() }

    fun checkUserLogin(){
        viewModelScope.launch {

        }
    }
}